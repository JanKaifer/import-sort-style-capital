import { IStyleAPI, IStyleItem } from 'import-sort-style';

export default function(styleApi: IStyleAPI): IStyleItem[] {
  const {
    and,
    hasMember,
    isAbsoluteModule,
    always,
    moduleName,
    name,
    naturally,
    not,
    startsWith,
    startsWithUpperCase,
  } = styleApi;

  const isReact = moduleName(startsWith('react'));

  const moduleStartsWithAt = moduleName(startsWith('@'));
  const moduleStartsWithUpperCase = and(
    moduleName(startsWithUpperCase),
    not(moduleStartsWithAt)
  );

  const isExternalModule = and(
    isAbsoluteModule,
    not(moduleStartsWithUpperCase)
  );
  const isLocalAbsoluteModule = and(
    isAbsoluteModule,
    moduleStartsWithUpperCase
  );

  return [
    /* node modules */
    {
      // React.js comes first
      match: and(isExternalModule, isReact),
      sort: moduleName(naturally),
      sortNamedMembers: name(naturally),
    },
    {
      match: and(isExternalModule, hasMember),
      sort: moduleName(naturally),
      sortNamedMembers: name(naturally),
    },

    // ---
    { separator: true },

    // local imports
    // import _ from "Foo"
    {
      match: and(isLocalAbsoluteModule, hasMember),
      sort: moduleName(naturally),
      sortNamedMembers: name(naturally),
    },

    // ---
    { separator: true },

    // import _ from "./foo"
    {
      match: and(hasMember),
      sort: moduleName(naturally),
      sortNamedMembers: name(naturally),
    },

    // ---
    { separator: true },

    // imports from node_modules
    // import "foo"
    {
      match: isExternalModule,
      sort: moduleName(naturally),
    },

    // local imports
    // import "Foo"
    {
      match: isLocalAbsoluteModule,
      sort: moduleName(naturally),
    },

    // import "./foo"
    {
      match: always,
      sort: moduleName(naturally),
    },
  ];
}
