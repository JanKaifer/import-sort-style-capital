import { A, b } from 'lib-a';

import { c, d } from 'LocalA';

import { e, f } from '../parent';
import { g, h, J } from './sibling';

code(A, b, c, d, e, f, g, h, J);
