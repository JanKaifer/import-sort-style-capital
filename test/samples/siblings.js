import c from 'lib-a';
import b from 'lib-b';

import d from 'LocalA';
import a from 'LocalB';

import g from '../parent';
import f from './sibling';
import e from './sibling-after';

code(a, b, c, d, e, f, g);
